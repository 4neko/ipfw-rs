#ipfw-rs ![600 logo](https://gitlab.com/4neko/ipfw-rs/-/raw/master/logo_600.png?ref_type=heads&inline=true)

A issues tracking is at https://gitlab.com/4neko/ipfw-rs

A crate which provides userspace interface to FreeBSD IPFW Firewall which allows to control IPFW directly without executing `ipfw(8)` every time when it is required to block network host or to check the list.

This crate supports only actual and recent FreeBSD version which is 14.0, but it seems that protocol did not changed much, so may work on FreeBSD 13!

## This is experimental crate and is used in "Aienma" project.

### At the moment the following is supported:
- Add, Remove, Test commands on tables (a list of hosts: IP/IPv6/DNS at the moment)
- Flush table


### Example

```rust


extern crate ipfw_rs;


use ipfw_rs::{Ipfw, IpfwCmd};

fn main() 
{
    let ipfw = Ipfw::new().unwrap();

    let res = 
        ipfw.ipfw_table_handler("testtable", IpfwCmd::Add { hosts: vec!["127.0.1.1", "127.0.2.0/24"], req_atomic_op: true }, false);

        match res
        {
            Ok(r) => println!("res: '{}'", r),
            Err(e) => println!("err: '{}'", e)
        }

    let res = 
        ipfw.ipfw_table_handler("testtable", IpfwCmd::Test { hosts: vec!["127.0.1.1", "127.0.2.0/24"]}, false);

        match res
        {
            Ok(r) => println!("res: '{}'", r),
            Err(e) => println!("err: '{}'", e)
        }

    
    let res = 
        ipfw.ipfw_table_handler("testtable", IpfwCmd::Flush, false); //{ hosts: vec!["127.0.1.2"] }, false);// { hosts: vec!["127.0.1.1", "127.0.2.0/24"], req_atomic_op: true }, false);

    match res
    {
        Ok(r) => println!("res: '{}'", r),
        Err(e) => println!("err: '{}'", e)
    }

    println!("Hello, world!");
}

````
